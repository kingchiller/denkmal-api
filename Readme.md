Denkmal API
===========

Local API development
---------------------
### 1: Run a postgres db
```
docker run -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -p 5432:5432 postgres:alpine
```
### 2: Install dependencies
```
yarn install
```

### 3: Run nodemon and ts-node
```
yarn start
```

The service runs at loalhost:5000 and nodemon watches for changes in the src directory.

Alternative: Docker execution
-----------------------------
### 1: Build docker images
```
docker-compose build
```
### 2: Start
```
docker-compose up
```


GraphiQL
--------

[http://localhost:5000/graphiql](http://localhost:5000/graphiql)