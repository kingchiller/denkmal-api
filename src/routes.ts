import * as Router from 'koa-router';
import * as koaBody from 'koa-bodyparser';
import {addCatchUndefinedToSchema, addMockFunctionsToSchema} from 'graphql-tools';

import {
    graphqlKoa,
    graphiqlKoa,
} from 'apollo-server-koa';


import { schema } from 'api/schema';
import {addMockData} from "./mock/addMockData";

export const routes = new Router();

const graphQlOpts = graphqlKoa({
    schema,
    context: {
        user: "guest"
    }
});

// API entrypoint
const apiEntrypointPath = '/graphql';

routes.get(apiEntrypointPath, graphQlOpts);
routes.post(apiEntrypointPath, koaBody(), graphQlOpts);

// GraphiQL entrypoint
routes.get('/graphiql', graphiqlKoa({ endpointURL: apiEntrypointPath }))

routes.get('/fillDatabaseWithMockData', async (ctx, next) => {
    await addMockData();
    ctx.body = "done";
});

routes.get('/', (ctx) => {
    ctx.body = "API endpoint: /graphql\nexplore: /graphiql"
})