import { eventResolver }  from './resolvers/eventResolver';
import { eventsResolver } from './resolvers/eventsResolver';
import { addRegionMutation } from "./mutations/addRegion";
import {regionResolver} from "./resolvers/regionResolver";
import {regionsResolver} from "./resolvers/regionsResolver";
import {changeRegionMutation} from "./mutations/changeRegion";
import {addVenueMutation} from "./mutations/addVenue";
import {venueResolver} from "./resolvers/venueResolver";
import {changeVenueMutation} from "./mutations/changeVenue";
import {changeEventMutation} from "./mutations/changeEvent";
import {deleteEventMutation} from "./mutations/deleteEvent";
import {eventDayResolver} from "./resolvers/eventDayResolver";
import {findSimilarEventsResolver} from "./resolvers/findSimilarEventsResolver";
import {dateResolver} from "./resolvers/dateResolver";
import {suggestEventMutation} from "./mutations/suggestEvent";
import {hideEventMutation} from "./mutations/hideEvent";

export const resolvers = {
    EventDay: eventDayResolver,
    Date: dateResolver,
    Query: {
        ...eventResolver,
        ...regionResolver,
        ...regionsResolver,
        ...venueResolver,
        ...findSimilarEventsResolver
    },
    Event: {
        ...venueResolver
    },
    Venue: {
        ...eventsResolver
    },
    Region: {
        ...eventsResolver
    },
    Mutation: {
        ...addRegionMutation,
        ...changeRegionMutation,
        ...suggestEventMutation,
        ...changeEventMutation,
        ...deleteEventMutation,
        ...addVenueMutation,
        ...changeVenueMutation,
        ...hideEventMutation
    }
};
