// lists all possible mutations
export const MutationType = `
    type Mutation {
        addRegion (
            id: ID
            name: String!
            email: String
            slug: String!
            latitude: Float!
            longitude: Float!
            timeZone    : String!
            suspendedUntil: Date
            facebookPage: String
            twitterAccount: String
            footerUrl   : String
        ): Region
        changeRegion (
            id: ID!
            name: String
            email: String
            slug: String
            latitude: Float
            longitude: Float
            timeZone    : String
            suspendedUntil: String
            facebookPage: String
            twitterAccount: String
            footerUrl   : String
        ): Region
        suggestEvent (
            id: ID
            title: String!
            description: String
            venueId: ID!
            from: Date!
            until: Date
        ): Event
        changeEvent (
            id: ID!
            title: String
            description: String
            from: Date
            until: Date
        ): Event
        hideEvent (
            id: ID!
            hide: Boolean
        ): Event
        deleteEvent (
            id: ID!
        ): Event
        addVenue (
            id: ID
            regionId    : ID!
            name        : String!
            address     : String
            url         : String
            email       : String
            longitude   : Float
            latitude    : Float
            isReviewPending: Boolean
            isSuspended : Boolean
            ignoreScraper: Boolean
            facebookPageId: String
            twitter     : String
            aliases     : [String]
        ): Venue
        changeVenue (
            id          : ID!
            name        : String
            address     : String
            url         : String
            email       : String
            longitude   : Float
            latitude    : Float
            isReviewPending: Boolean
            isSuspended : Boolean
            ignoreScraper: Boolean
            facebookPageId: String
            twitter     : String
            aliases     : [String]
        ): Venue
    }
`;
