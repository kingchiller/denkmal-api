export const EventVariantType = `
    type EventVariant {
        id          : ID
        title       : String
        description : String
        from        : Date!
        until       : Date
        isReviewPending: Boolean
        createdAt  : Date
    }
`;