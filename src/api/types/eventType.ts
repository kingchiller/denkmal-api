export const EventType = `
    type Event {
        id          : ID
        title       : String
        description : String
        venue       : Venue
        variants    : [EventVariant]
        from        : Date!
        until       : Date
        isReviewPending: Boolean
        isHidden    : Boolean
        createdAt  : Date
    }
`;
