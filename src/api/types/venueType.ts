import {events} from "./commonFields";

export const VenueType = `
    type Venue {
        id          : ID!
        name        : String!
        address     : String
        url         : String
        email       : String
        createdAt  : Date
        updatedAt  : Date
        latitude    : Float
        longitude   : Float
        isQueued    : Boolean
        isSuspended : Boolean
        ignoreScraper: Boolean
        facebookPageId: String
        twitter     : String
        aliases     : [String]
        region      : Region
        ${events}
    }
`;
