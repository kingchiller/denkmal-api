// lists all possible queries
import {events} from "./commonFields";

export const QueryType = `
    type Query {
        regions: [Region]
        region(id: ID, slug: String): Region
        venue(id: ID!): Venue
        event(id: ID!): Event
        findSimilarEvents(venueId: String!, from: String!): [Event]
    }
`;