import {events} from "./commonFields";

export const RegionType = `
    type Region {
        id          : ID!
        name        : String!
        email       : String
        slug        : String!
        createdAt  : Date
        updatedAt  : Date
        longitude   : Float
        latitude    : Float
        timeZone    : String!
        dayOffset   : Float
        suspendedUntil: String
        facebookPage: String
        twitterAccount: String
        footerUrl   : String
        venues      : [Venue]
        ${events}
    }
`;