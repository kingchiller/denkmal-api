import {getCustomRepository, getManager} from 'typeorm';
import { Event } from 'entities/event';
import {EventVersionRepository} from "../../repository/eventVersionRepository";
import {EventRepository} from "../../repository/eventRepository";
import {EventVersion} from "../../entities/eventVersion";
import {ApiEvent} from "../helpers/apiEvent";


export const changeEventMutation = {
    async changeEvent(_, attrs) {
        /*
            creates new version
         */

        const eventRepository = getCustomRepository(EventRepository);
        const event = await eventRepository.findOneOrFail(attrs.id);

        const eventVersion = event.activeVersion;

        const eventVersionRepository = getCustomRepository(EventVersionRepository);

        const newEventVersion = await eventVersionRepository.save(Object.assign(
            new EventVersion(),
            {
                ...eventVersion
            },
            {
                ...attrs
            }
        ));

        event.activeVersion = newEventVersion;
        await eventRepository.save(event);

        return ApiEvent.apiEventForEvent(event);
    }
};
