import {getCustomRepository, getManager} from "typeorm";
import { Event }          from 'entities/event';
import { Venue }          from 'entities/venue';
import {EventRepository} from "../../repository/eventRepository";
import {ApiEvent} from "../helpers/apiEvent";
import {EventVersionRepository} from "../../repository/eventVersionRepository";
import {EventVersion} from "../../entities/eventVersion";


export const suggestEventMutation = {
    async suggestEvent(_, attrs) {
        const entityManager = getManager();
        const venue = await entityManager.findOneOrFail(Venue, { id: attrs.venueId });

        const eventRepository = getCustomRepository(EventRepository);
        const from = attrs.from;

        const similar = await eventRepository.findSimilar(venue.id, from)

        console.log(similar);
        var event = null;

        if (similar && similar.length > 0) {
            event = similar[0];
        } else {
            const eventObj = {
                venue: Promise.resolve(venue)
            }
            event = await eventRepository.save(Object.assign(new Event(), eventObj));
        }

        // create version
        const versionObj = {
            ...attrs,
            isReviewPending: true,
            event: Promise.resolve(event)
        }

        const versionRepository = getCustomRepository(EventVersionRepository);
        var version = await versionRepository.save(Object.assign(new EventVersion(), versionObj));

        event.activeVersion = version;
        await eventRepository.save(event);

        // convert to api event
        return ApiEvent.apiEventForEvent(event);
    }
};
