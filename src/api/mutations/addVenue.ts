import {getConnection, getManager} from 'typeorm';
import {Venue} from 'entities/venue';
import {Region} from "entities/region";


export const addVenueMutation = {
    async addVenue(_, attrs) {
        const entityManager = getManager();
        const region = await entityManager.findOneOrFail(Region, { id: attrs.regionId });
        const venue = {
            ...attrs,
            region: Promise.resolve(region)
        }


        return await entityManager.save(Object.assign(new Venue(), venue));
    }
};