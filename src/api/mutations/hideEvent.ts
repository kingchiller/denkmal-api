import {getCustomRepository} from "typeorm";
import {EventRepository} from "../../repository/eventRepository";
import {ApiEvent} from "../helpers/apiEvent";

export const hideEventMutation = {
    async hideEvent(_, attrs) {
        const eventRepository = getCustomRepository(EventRepository);
        const event = await eventRepository.findOneOrFail(attrs.id);

        event.isHidden = attrs.hide == true;

        await eventRepository.save(event);

        return ApiEvent.apiEventForEvent(event);
    }
}