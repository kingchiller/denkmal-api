import {getManager} from "typeorm";
import {Venue} from "../../entities/venue";

export const changeVenueMutation = {
    async changeVenue(_, attrs) {
        const entityManager = getManager();
        const venueId = attrs.id;
        const venue = await entityManager.findOneOrFail(Venue, { id: venueId });

        return await entityManager.save(Venue, {
            ...venue,
            ...attrs
        });
    }
}