import {getConnection, getCustomRepository, getManager, getRepository} from "typeorm";
import {EventRepository} from "../../repository/eventRepository";
import {ApiEvent} from "../helpers/apiEvent";

/*
    This is used for testing caching only
 */
export const allEventsResolver = {
    async allEvents(obj, args, context, info) {

        const eventRepo = getCustomRepository(EventRepository);
        const events = await eventRepo.find();

        return events.map(async e => await ApiEvent.apiEventForEvent(e));
    }
};