import {getCustomRepository} from "typeorm";
import {ApiEvent} from "../helpers/apiEvent";
import {EventRepository} from "../../repository/eventRepository";


export const eventResolver = {
    async event(obj, { id }, context, info) {
        const repository = getCustomRepository(EventRepository);
        const event = await repository.findOneOrFail(id);

        return ApiEvent.apiEventForEvent(event);
    }
};
