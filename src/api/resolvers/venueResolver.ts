import {getCustomRepository} from "typeorm";
import {VenueRepository} from "../../repository/venueRepository";


export const venueResolver = {
    async venue(obj, { id }, context, info) {
        var searchId = id;
        if (obj && obj.constructor.name == "ApiEvent") {
            searchId = obj.event.venueId;
        }
        const repository = getCustomRepository(VenueRepository);
        return await repository.findOneOrFail(searchId);
    }
};
