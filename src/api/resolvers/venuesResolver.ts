import {getCustomRepository} from "typeorm";
import {VenueRepository} from "../../repository/venueRepository";

export const venuesResolver = {
    async venues() {
        const repository = getCustomRepository(VenueRepository);
        return await repository.find();
    }
};
