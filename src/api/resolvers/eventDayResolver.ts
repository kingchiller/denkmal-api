import {GraphQLScalarType, Kind} from "graphql";
import moment = require("moment");


export const eventDayResolver = new GraphQLScalarType({
    name: 'EventDay',
    description: 'Date with format: YYYY-MM-DD',
    serialize(value) {
        let result;
        // Implement custom behavior by setting the 'result' variable
        return result;
    },
    parseValue(value) {
        let result = moment(value, "YYYY-MM-DD", true);
        // Implement custom behavior here by setting the 'result' variable
        return result;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                const m =  moment(ast.value as string, "YYYY-MM-DD", true);
                if (m && m.isValid()) {
                    return m;
                }
        }
    }
});