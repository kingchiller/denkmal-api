import {getCustomRepository} from "typeorm";
import {RegionRepository} from "../../repository/regionRepository";


export const regionsResolver = {
    async regions(obj, args, context, info) {
        const repository = getCustomRepository(RegionRepository);
        return await repository.find();
    }
};
