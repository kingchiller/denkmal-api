import {getConnection, getCustomRepository, getManager, getRepository} from "typeorm";
import {EventRepository} from "../../repository/eventRepository";
import {ApiEvent} from "../helpers/apiEvent";
import {RegionRepository} from "../../repository/regionRepository";
import {Region} from "../../entities/region";
import {Venue} from "../../entities/venue";

export const eventsResolver = {
    async events(obj, args, context, info) {
        var venue : Venue = null;
        var region : Region = null;
        if (obj && obj.constructor.name == "Venue") {
            region = await getCustomRepository(RegionRepository).findOneOrFail(obj.regionId);
            venue = obj as Venue;
        }

        if (obj && obj.constructor.name == "Region") {
            region = obj as Region;
        }

        const events = await getCustomRepository(EventRepository).findByEventDay(args.eventDay, region, venue, args.withHidden);

        return events.map(async e => await ApiEvent.apiEventForEvent(e));
    }
};
