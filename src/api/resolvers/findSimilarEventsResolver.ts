import {getCustomRepository} from "typeorm";
import {VenueRepository} from "../../repository/venueRepository";
import {EventRepository} from "../../repository/eventRepository";
import moment = require("moment");
import {ApiEvent} from "../helpers/apiEvent";


export const findSimilarEventsResolver = {
    async findSimilarEvents(obj, args, context, info) {
        const repository = getCustomRepository(VenueRepository);
        const venue = await repository.findOneOrFail(args.venueId);

        const eventRepository = getCustomRepository(EventRepository);
        const events = await eventRepository.findSimilar(args.venueId, moment(args.from));

        if (events) {

            events.forEach(e => {
                console.log(e.activeVersionId);
            });
            //return events.map(async e => await ApiEvent.apiEventForEventVersion(e.activeVersion, e));
        }
    }
}