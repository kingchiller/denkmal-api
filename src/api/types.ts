import { EventType } from './types/eventType';
import { RegionType } from './types/regionType';
import { VenueType } from './types/venueType';
import {EventDayType} from "./types/eventDayType";
import {DateType} from "./types/dateType";
import {EventVariantType} from "./types/eventVariantType";

export const types = [
    RegionType,
    EventType,
    VenueType,
    EventDayType,
    DateType,
    EventVariantType
];