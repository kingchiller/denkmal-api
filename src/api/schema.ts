import { makeExecutableSchema } from 'graphql-tools';
import { MutationType }  from './types/mutationType';
import { QueryType }     from './types/queryType';
import { types }     from './types';
import { resolvers } from './resolvers';

const schemaDefinition = `
    schema {
        query: Query
        mutation : Mutation 
    }
`;

const typeDefs = [
    schemaDefinition,
    QueryType,
    MutationType,
    ...types
];

export const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});
