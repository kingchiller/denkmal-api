import {Venue} from "../../entities/venue";
import {Event} from "../../entities/event";
import {AbstractApiEvent} from "./abstractApiEvent";
import {ApiEventVariant} from "./apiEventVariant";

export class ApiEvent extends AbstractApiEvent {
    isHidden: boolean;

    // internal
    event: Event;

    async venue() : Promise<Venue> {
        return this.event.venue;
    }

    /* TODO: make a separate API object called EventVariant to create a clear hiearchy:
        ApiEvent
            variants: [EventVariant]

        where an eventVariant has no variants field
    */
    async variants() : Promise<ApiEventVariant[]> {
        const versions = await this.event.versions;

        var variantsArray = [] as [ApiEventVariant];
        for (var versionI = 0; versionI < versions.length; versionI++) {
            const v = versions[versionI];

            if (v.id != this.event.activeVersionId) {
                const apiEventVariant = await ApiEventVariant.apiEventVariantForEventVersion(v);
                variantsArray.push(apiEventVariant);
            }
        }

        return variantsArray;
    }

    static async apiEventForEvent(event: Event) : Promise<ApiEvent>  {
        const activeVersion = await event.activeVersion;
        const apiEvent = new ApiEvent();
        apiEvent.id = event.id;
        apiEvent.createdAt = event.createdAt;
        apiEvent.from = activeVersion.from;
        apiEvent.until = activeVersion.until;
        apiEvent.title = activeVersion.title;
        apiEvent.description = activeVersion.description;
        apiEvent.isReviewPending = activeVersion.isReviewPending;
        apiEvent.isHidden = event.isHidden;
        apiEvent.event = event;
        return apiEvent;
    }
}