import {Event} from "../../entities/event";
import eventVersionmoment = require("moment");
import {AbstractApiEvent} from "./abstractApiEvent";
import {EventVersion} from "../../entities/eventVersion";
import {Venue} from "../../entities/venue";
import {ApiEvent} from "./apiEvent";

export class ApiEventVariant extends AbstractApiEvent {
    eventVersion: EventVersion;

    _event?: Event;
    async event() : Promise<ApiEvent> {
        if (!this._event) {
            this._event = await this.eventVersion.event;
        }
        console.log(this._event)
        return await ApiEvent.apiEventForEvent(this._event);
    }

    static async apiEventVariantForEventVersion(eventVersion: EventVersion, event?: Event) : Promise<ApiEventVariant> {
        const eventVariant = new ApiEventVariant();
        eventVariant.id = eventVersion.id;
        eventVariant.createdAt = eventVersion.createdAt;
        eventVariant.from = eventVersion.from;
        eventVariant.until = eventVersion.until;
        eventVariant.title = eventVersion.title;
        eventVariant.description = eventVersion.description;
        eventVariant.isReviewPending = eventVersion.isReviewPending;
        eventVariant.eventVersion = eventVersion;
        eventVariant._event = event;

        return eventVariant;
    }
}