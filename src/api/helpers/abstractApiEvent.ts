import moment = require("moment");

export abstract class AbstractApiEvent {
    id: string;
    createdAt: moment.Moment;
    from: moment.Moment;
    until: moment.Moment;
    title: string;
    description: string;
    isReviewPending: boolean;
}