import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany
} from 'typeorm';
import {Venue} from "./venue";
import moment = require("moment-timezone");
import {IsEmail} from "class-validator";
import {sharedDateTransformer} from "./transformers/dateTransformer";

@Entity('regions')
export class Region {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: moment.Moment;

    @Column('text')
    name: string;

    @Column('text', {nullable: true})
    @IsEmail()
    email: string;

    @Column('text', {unique: true})
    slug: string;

    @Column('real', {nullable: true})
    latitude: number;

    @Column('real', {nullable: true})
    longitude: number;

    @Column('text', {nullable: true})
    timeZone: string;

    @Column('real', {default: 0})
    dayOffset: number;

    @Column('timestamp', {nullable: true, transformer: sharedDateTransformer})
    suspendedUntil: moment.Moment;

    @Column('text', {nullable: true})
    facebookPage: string;

    @Column('text', {nullable: true})
    twitterAccount: string;

    @Column('text', {nullable: true})
    footerUrl: string;

    @OneToMany(type => Venue, venue => venue.region)
    venues: Promise<Venue[]>;


    /*
    gets the start and end moments from an eventDay
     */
    getEventDayRange(eventDay: moment.Moment) : [moment.Moment, moment.Moment] {
        const timeZoneDate = moment.tz(this.timeZone)
            .set({
                year: eventDay.year(),
                month: eventDay.month(),
                date: eventDay.date(),
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0});

        const startUtc = moment(timeZoneDate).startOf("day").add(this.dayOffset, 'hour');
        const endUtc = moment(timeZoneDate).endOf("day").add(this.dayOffset, 'hour')

        return [
            startUtc,
            endUtc];
    }
}
