import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    Index, OneToOne, ManyToOne, RelationId
} from 'typeorm';
import {Event} from "./event";
import moment = require("moment");
import {DateTransformer, sharedDateTransformer} from "./transformers/dateTransformer";

@Entity('eventVersions')
export class EventVersion {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @Column('timestamp', {transformer: sharedDateTransformer})
    @Index()
    from: moment.Moment;

    @Column('timestamp', {nullable: true, transformer: sharedDateTransformer})
    @Index()
    until: moment.Moment;

    @Column('text', {nullable: true})
    title: string;

    @Column('text', {nullable: true})
    description: string;

    /*
    formerly called "queued"
    */
    @Column('boolean', {default: true, nullable: true})
    isReviewPending: boolean;

    @ManyToOne(type => Event, event => event.versions, {nullable: false})
        @Index()
    event: Promise<Event>;

    @RelationId((eventVersion: EventVersion) => eventVersion.event)
    eventId: string;
}
