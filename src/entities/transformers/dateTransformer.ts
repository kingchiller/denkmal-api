import {ValueTransformer} from "typeorm/decorator/options/ValueTransformer";
import moment = require("moment");

export class DateTransformer implements ValueTransformer {
    to(value: any) {
        // from moment to timestamp
        const mo = value as moment.Moment;
        return value;
        return mo.unix();
    }

    from(value: any) {
        // from timestamp to moment
        const mo = moment(value);
        return mo;
    }
}

export const sharedDateTransformer = new DateTransformer();