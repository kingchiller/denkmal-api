import {getCustomRepository} from "typeorm";
import {RegionRepository} from "../repository/regionRepository";
import {Region} from "../entities/region";
import casual = require("casual");
import {VenueRepository} from "../repository/venueRepository";
import {Venue} from "../entities/venue";
import {EventRepository} from "../repository/eventRepository";
import moment = require("moment");
import {EventVersionRepository} from "../repository/eventVersionRepository";
import {EventVersion} from "../entities/eventVersion";
import {Event} from "../entities/event";

export const addMockData = async function() {
    await addMockRegions();
    await addMockVenues();
    await addMockEvents();
}

export const addMockRegions = async function() {
    const regionRepository = getCustomRepository(RegionRepository);
    for (let i = 1; i <= 3; i++) {
        const r = new Region();
        const city = casual.city;
        r.name = city;
        r.slug = casual.word + i;
        r.latitude = parseFloat(casual.latitude);
        r.longitude = parseFloat(casual.longitude);
        r.email = casual.email;
        r.name = casual.city;
        r.timeZone = casual.timezone;
        r.dayOffset = casual.integer(0, 5);

        await
            regionRepository.save(r);
    }
}

export const addMockVenues = async function() {
    const regionRepository = getCustomRepository(RegionRepository);
    const venueRepository = getCustomRepository(VenueRepository);

    const regions = await regionRepository.find();

    for (var i=0; i<regions.length; i++) {
        const r = regions[i];
        const venuesToGenerate = casual.integer(10, 20);

        for (let i = 1; i <= venuesToGenerate; i++) {
            const v = new Venue();
            v.name = casual.company_name;
            v.address = casual.address;
            v.email = casual.email;
            v.latitude = parseFloat(casual.latitude);
            v.longitude = parseFloat(casual.longitude);
            v.region = Promise.resolve(r);
            v.url = casual.url;

           await venueRepository.save(v);
        }
    }
}

export const addMockEvents = async function() {
    const eventRepository = getCustomRepository(EventRepository);
    const regionRepository = getCustomRepository(RegionRepository);
    const eventVersionRepository = getCustomRepository(EventVersionRepository);



    const startDate = moment("2018-08-31");
    const endDate = moment("2018-09-30");
    var days : moment.Moment[] = [];

    var currDate = startDate;
    while(endDate.diff(currDate) > 0) {
        days.push(currDate);
        currDate = moment(currDate).add(1, 'day');
    }


    const regions = await regionRepository.find();


    for (var regionI = 0; regionI < regions.length; regionI++) {
        const region = regions[regionI];
        const venues = await region.venues;

        for (var venueI = 0; venueI < venues.length; venueI++) {
            const v = venues[venueI];
            const eventsPerDayProbability = Math.random();

            for(var dayI = 0; dayI<days.length; dayI++) {
                const d = days[dayI];
                const hasEvent = eventsPerDayProbability > Math.random();

                if (hasEvent) {
                    const rawEvent = new Event();
                    rawEvent.venue = Promise.resolve(v);
                    rawEvent.isHidden = Math.random() > 0.95;
                    rawEvent.regionId = region.id;
                    const event = await eventRepository.save(rawEvent);

                    const date = moment(d).add(Math.random()*24, 'hour');
                    const rawMainVersion = new EventVersion();
                    rawMainVersion.title = casual.catch_phrase;
                    rawMainVersion.description = casual.short_description;
                    rawMainVersion.from = date;
                    rawMainVersion.event = Promise.resolve(event);
                    rawMainVersion.isReviewPending = Math.random() > 0.95;
                    if (Math.random() > 0.75) {
                        rawMainVersion.until = moment(date).add((Math.random())*120, 'minute')
                    }
                    const mainVersion = await eventVersionRepository.save(rawMainVersion);

                    event.activeVersion = mainVersion;
                    await eventRepository.save(event);

                    var additionalVersions = casual.integer(0,Math.random() > 0.95 ? 15: 3);

                    for (var i=0; i<additionalVersions; i++) {
                        const version = new EventVersion();
                        version.title = rawMainVersion.title + " (" + casual.integer(1, 10000) + ")",
                        version.description = Math.random() > 0.8 ? casual.short_description : rawMainVersion.description,
                        version.from = Math.random() > 0.5 ? moment(rawMainVersion.from).add(casual.integer(1, 120), 'minutes') : rawMainVersion.from,
                        version.event = Promise.resolve(event);
                        version.isReviewPending = Math.random() > 0.95;
                        version.until = rawMainVersion.until;
                        version.event = Promise.resolve(event);

                        if (version.until && version.until.isSameOrBefore(version.from)) {
                            version.until = moment(version.from).add((Math.random())*120, 'minutes');
                        }

                        await eventVersionRepository.save(version);
                    }
                }
            }
        }
    }
}