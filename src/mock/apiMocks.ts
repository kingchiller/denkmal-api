import * as casual from 'casual';
import {MockList} from "graphql-tools";
export const apiMocks = {

    Region: () => ({
        id: casual.uuid,
        name: casual.word,
        email: casual.email,
        slug: casual.word,
        updated_at: casual.unix_time,
        created_at: casual.unix_time,
        events: () => new MockList([0, 20]),
    }),

    Coords: () => ({
        latitude: casual.latitude,
        longitude: casual.longitude
    }),

    Event: () => ({
        id: casual.uuid,
        description: casual.description,
        updated_at: casual.unix_time,
        created_at: casual.unix_time,
        from: () => {
            const dayshift = Math.random() * 10 - 2;
            const now = new Date();
            now.setDate(now.getDate() + dayshift);
            return (now.getTime() /1000).toFixed(0);
        }
    }),

    Venue: () => ({
        id: casual.uuid,
        name: casual.title,
        url: casual.url,
        email: casual.email,
        address: casual.address,
        updated_at: casual.unix_time,
        created_at: casual.unix_time
    })
}