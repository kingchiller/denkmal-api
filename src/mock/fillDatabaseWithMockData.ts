import {addMockData, addMockEvents, addMockRegions, addMockVenues} from "./addMockData";
import {Database} from "../database/database";


const database = new Database(process.env.DATABASE_URL);
database.connect(false).then(async connection => {
    await database.clearAllData();
    await addMockData();
    await connection.close();
});