import {Venue} from "../entities/venue";
import {Region} from "../entities/region";
import {Event} from "../entities/event";
import {URL} from 'url';

import {Connection, createConnection, getConnection, getRepository} from 'typeorm';
import {EventVersion} from "../entities/eventVersion";
import {ConnectionOptions} from "typeorm/connection/ConnectionOptions";

export class Database {
    host: string = process.env.DATABASE_HOST || 'localhost';
    port: number = 5432;
    username: string = 'postgres';
    password: string = 'postgres';
    database: string = 'postgres';
    connection: Promise<Connection>;

    constructor(connectionString?: string) {
        if (connectionString) {
            const url = new URL(connectionString);
            this.host = url.hostname;
            this.port = parseInt(url.port);
            this.username = url.username;
            this.password = url.password;
            this.database = url.pathname && url.pathname.substr(1);
        }
    }

    async connect(enableCaching : Boolean) : Promise<Connection> {
        var options = {
            type     : 'postgres',
            host     : this.host,
            port     : this.port,
            username : this.username,
            password : this.password,
            database : this.database,
            entities : [
                Venue,
                Region,
                Event,
                EventVersion
            ],
            logging: ['query', 'error'],
            synchronize: true,

        };
        if (enableCaching) {
            // TODO pass caching config to connect function
            options["cache"] = {
                type: "redis",
                options: {
                    host: "localhost",
                    port: 6379
                }
            }
        }
        this.connection = createConnection(options as ConnectionOptions);
        this.connection.then((...args) => {
            console.log('Database connection established');
        }).catch(e => {
            console.error("Could not connect to database");
            console.error(e);
            process.exit(1);
        });
        return this.connection;
    }

    async getEntities() {
        const entities = [];
        (await (await getConnection().entityMetadatas).forEach(
            x => entities.push({name: x.name, tableName: x.tableName})
        ));
        return entities;
    }

    async clearAll(entities) {
        try {
            const connection = await this.connection;
            for (const entity of entities) {
                connection.query(`TRUNCATE TABLE "${entity.tableName}" CASCADE`);
            }
        } catch (error) {
            throw new Error(`ERROR: Cleaning test db: ${error}`);
        }
    }

    async clearAllData() {
        await this.clearAll(await this.getEntities());
    }
}