import * as Koa from 'koa';
import { routes } from 'routes'
import {Database} from "./database/database";

const server = async () : Promise<Database> => {
    process.env.TZ='UTC';

    const database = new Database(process.env.DATABASE_URL);
    await database.connect(false);

    const app = new Koa();
    app
        .use(routes.routes())
        .use(routes.allowedMethods())
        .listen(process.env.PORT || 5000);

    return database;
}

server();