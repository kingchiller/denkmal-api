import {EntityRepository, getCustomRepository, Repository} from "typeorm";
import {Event} from "../entities/event";
import {VenueRepository} from "./venueRepository";
import {Moment} from "moment";
import {RegionRepository} from "./regionRepository";
import {Region} from "../entities/region";
import {Venue} from "../entities/venue";
import moment = require("moment");

@EntityRepository(Event)
export class EventRepository extends Repository<Event> {

    async findByEventDay(eventDay: moment.Moment, region: Region, venue: Venue = null, withHidden: boolean = false) {
        var query = this
            .createQueryBuilder("event")
            .innerJoinAndSelect("event.activeVersion", "activeVersion");

        query.andWhere('event.regionId = :regionId', {regionId: region.id});

        if (!withHidden) {
            query.andWhere('event.isHidden = false');
        }

        if (venue) {
            query.andWhere("event.venue_id = :id", {id: venue.id});
        }

        if (eventDay) {
            const [fromDate, untilDate] = region.getEventDayRange(eventDay);

            query.andWhere("activeVersion.from >= :fromDate", {fromDate: fromDate.toDate()});
            query.andWhere("activeVersion.from <= :untilDate", {untilDate: untilDate.toDate()});
        }

        const events = await query
            .getMany();

        return events;
    }

    async findSimilar(venueId: string, from: Moment) : Promise<Event[]> {
        const venueRepository = getCustomRepository(VenueRepository);
        const venue = await venueRepository.findOne(venueId);
        if (!venue) {
            return;
        }

        const region = await venue.region;
        var [dayStart, dayEnd] = region.getEventDayRange(from);

        const eventRepository = this.createQueryBuilder("event");
        return await eventRepository.innerJoinAndSelect("event.activeVersion", "activeVersion")
            .andWhere("event.venue_id = :venueId", {venueId: venueId})
            .andWhere("activeVersion.from BETWEEN :dayStart::timestamp AND :dayEnd::timestamp",
                {dayStart: dayStart.toISOString(), dayEnd: dayEnd.toISOString()})
            .orderBy("activeVersion.from", "DESC")
            .getMany();
    }
}