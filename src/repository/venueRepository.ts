import {EntityRepository, Repository} from "typeorm";
import {Venue} from "../entities/venue";

@EntityRepository(Venue)
export class VenueRepository extends Repository<Venue> {

}